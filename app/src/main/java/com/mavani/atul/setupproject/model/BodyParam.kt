package com.example.atul.attendance.model

import org.json.JSONArray

data class BodyParam (var email : String, var password : String, var device_id : String, var currencyCode : String)